/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2015 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class lanStub {
static int portNumber = 4711;
static boolean Running = true;

  public lanStub() {
    new lanServer().start();
  }
  
  public static void main(String[] args) {
    new lanStub();
  }

  class lanServer extends Thread {
    public void run() {
      try {
        ServerSocket serverSocket = new ServerSocket(portNumber);
        while(Running) {
          System.out.println("Waiting for client connection...");
          Socket clientSocket = serverSocket.accept();
          System.out.println("Client connected: " + clientSocket.getInetAddress());
          Connection connection = new Connection();
          connection.run = true;
          connection.socket = clientSocket;
          new lanReader(connection).start();
          new lanWriter(connection).start();
          Thread.sleep(100);
        }
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  class Connection {
    Socket socket = null;
    boolean run;
  }

  /*
   * Read incoming RCP commands in form of XML strings and try to parse them.
   */
  class lanReader extends Thread {
    Connection connection;
    public lanReader(Connection connection) {
      this.connection = connection; 
    }
    public void run() {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      System.out.println("Client reader started for " + connection.socket.getInetAddress());
      try {
        BufferedReader in = new BufferedReader( new InputStreamReader(connection.socket.getInputStream()));
        while(connection.run && connection.socket.isConnected()) {
          String xmlStr = in.readLine();
          if( xmlStr == null ) {
            connection.run = false;
            connection.socket.close();
            break;
          }
          System.out.println("in: [" + xmlStr + "]");
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          InputStream stream = new ByteArrayInputStream(xmlStr.getBytes("UTF-8"));
          Document doc = dBuilder.parse(stream);
          
          // The root node name determines the command type, see:
          // http://rocrail.net/software/rocrail-snapshot/rocrail/wrapper-en-index.html
          String cmdType = doc.getDocumentElement().getNodeName();
          System.out.println("root: " + cmdType);
          
          Element cmd = doc.getDocumentElement();
          if( cmdType.equals("lc")) {
            // Handle a loco command.
            System.out.println("handle loco command: cmd="+ cmd.getAttribute("cmd") + " V=" + cmd.getAttribute("V") + " dir=" + cmd.getAttribute("dir"));
          }
          else if( cmdType.equals("sw")) {
            // Handle a switch command.
            System.out.println("handle switch command: cmd=" + cmd.getAttribute("cmd"));
          }
          else if( cmdType.equals("co")) {
            // Handle an output command.
            System.out.println("handle output command: cmd=" + cmd.getAttribute("cmd"));
          }
          else if( cmdType.equals("sg")) {
            // Handle an output command.
            System.out.println("handle signal command: cmd=" + cmd.getAttribute("cmd"));
          }
          
          Thread.sleep(100);
        }
      }
      catch(Exception e) {
        e.printStackTrace();
      }
      System.out.println("Client reader ended");
    }
  }
  
  class lanWriter extends Thread {
    Connection connection;
    public lanWriter(Connection connection) {
      this.connection = connection; 
    }
    public void run() {
      System.out.println("Client writer started for " + connection.socket.getInetAddress());
      try {
        boolean reported = false;
        PrintWriter out = new PrintWriter(connection.socket.getOutputStream(), true);
        while(connection.run && connection.socket.isConnected()) {
          if( !reported ) {
            Thread.sleep(10000);
            // Send a sensor event.
            // In this case the id is the read RFID by a reader. The fbtype 5 is RFID.
            String xmlStr = "<fb id=\"102.567.22.46\" state=\"true\" fbtype=\"5\"/>";
            out.println(xmlStr);
            reported = true;
          }
          Thread.sleep(100);
        }
      }
      catch(Exception e) {
        e.printStackTrace();
      }
      System.out.println("Client writer ended");
    }
  }

}
